/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.keti.tas.soft.edu;

import com.keti.tas.soft.ByteUtil;

/**
 *
 * @author ChenNan
 */
public class BoardCommandParser {
    public byte[] makeByteCommand(String target, String comm){
        
    	String targetId = "01 B4 01 01 01 01 01 01";
        String targetAction = "";
		
        switch(target)
        {
        case "led":
                //
                if ("on".equals(comm)) {
                    targetAction = "5";
                }
                else if ("off".equals(comm)) {
                    targetAction = "4";
                }
                else {
                	return null;
                }
                break;

        default:
                System.out.println("target not found");
                return null;
        }
        return controlActuator(targetId, targetAction);

    }
    
    private byte[] controlActuator(String appId, String action) {
        byte[] packet = new byte[16];

        packet[0] = 0x78;
        packet[1] = 0x00;
        packet[2] = 0x00;
        packet[3] = 0x70;
        packet[4] = 0x09;
        packet[5] = (byte)Integer.parseInt(action,16);

        byte[] address = ByteUtil.toByteArray(appId);
        
        for(int i = 0; i < address.length; i++) {
                packet[6+i] = address[i];
        }
        packet[14] = 0x00;

        return packet;
    }
}

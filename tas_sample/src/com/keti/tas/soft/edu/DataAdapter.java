/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.keti.tas.soft.edu;

import com.keti.tas.soft.AdaptorInfomation;
import com.keti.tas.soft.ByteUtil;
import com.keti.tas.soft.CubeTasClient;
import com.keti.tas.soft.CubeTasServer;
import com.keti.tas.soft.KoreaTimeZone;
import com.keti.tas.soft.MsgReceiveEvent;
import com.keti.tas.soft.MsgReceiveListener;
import com.keti.tas.soft.ThingData;
import com.keti.tas.soft.ThingInformation;
import com.keti.tas.soft.ThingTasConnector;
import com.keti.tas.soft.ThingsManagement;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ChenNan
 */
public class DataAdapter {
    
    public static void main(String argString[]) throws IOException, InterruptedException{
        
        final CubeTasServer cubeServer = new CubeTasServer();
        final ThingTasConnector thingServer = new ThingTasConnector();
        
        cubeServer.start();
        thingServer.start();
  
        AdaptorInfomation adaptor = new AdaptorInfomation(cubeServer.getServerPort());
        ThingsManagement manager = new ThingsManagement(adaptor);
        
        manager.addThing("ledstate", "sensoractor" ,"This is a control board!");    
        manager.addControlThing("ledcontrol", "sensoractor", "This is a control board!", cubeServer.getServerPort());

        thingServer.addReceiveListener(new MsgReceiveListener() {

            @Override
            public void receiveMsgEvent(MsgReceiveEvent event) {
                 String strData = event.getMessage();
                 
                 System.out.println(strData);
                 
                 if(strData.length() >= 15){
        	 
                	 byte Id[] = ThingTasConnector.hexToByteArray(strData.substring(6, 8));
                     byte dios[] = ThingTasConnector.hexToByteArray(strData.substring(22, 24));
                     dios[0] = (byte) (dios[0] << 4);
                     dios[0] = (byte) (dios[0] >>> 4);
                     
                     byte dio1 = (byte) (dios[0] << 6);
                     dio1 = (byte) (dio1 >>> 6);
                     
                     int IdVal = ByteUtil.byteToInt(Id[0]);
                     int dio1Val = ByteUtil.byteToInt(dio1);

                     System.out.println(KoreaTimeZone.getDisplayTimeNow() + "=>");
                     System.out.println("ID: " + IdVal);
                     System.out.println("dioVal: " + dio1Val);
                     System.out.println("============================");

                     StringBuilder builder = new StringBuilder();

                     builder.append(dio1Val);

                     ThingData mData = new ThingData();
                   
                     try {
                         ThingInformation thing = ThingsManagement.getThingIDByName("ledstate");
                         if(thing != null && thing.getRegistStatus())
                         new CubeTasClient().upload( mData.makeThingDataMsg(thing.getThingName(), "onoffstate", builder.toString()));
                     } catch (IOException ex) {
                         Logger.getLogger(DataAdapter.class.getName()).log(Level.SEVERE, null, ex);
                     }

                 }
            }
        });
        
      //Add a receive listener to CubeServer
        cubeServer.addReceiveListener(new MsgReceiveListener() { 

            @Override
            public void receiveMsgEvent(MsgReceiveEvent event) {
                //Generate control meessage and send it to Thing
                System.out.println(KoreaTimeZone.getDisplayTimeNow()+ " => Receive a message {" + event.getMessage() + "} from &CUBE");
                
                if(event.getMessage().length() > 0){
                    String value[] = event.getMessage().split(",");
                    
                    if(value.length == 3) {
                        String thingName = value[1];
                        String action = value[2];
                        
                        BoardCommandParser parser = new BoardCommandParser();
                        byte[] comm = parser.makeByteCommand(thingName, action);
                        
                        if(comm != null){
                        
                            System.out.println(KoreaTimeZone.getDisplayTimeNow()+ " => Send a command {" + ThingTasConnector.byteArrayToHex(comm) + "} to Thing");

                            thingServer.sendToThing(comm);

                            System.out.println(KoreaTimeZone.getDisplayTimeNow()+ " => Send a command {" + new String(comm) + "} to Thing");
                        }
                        else{
                            System.out.println(KoreaTimeZone.getDisplayTimeNow()+ " => Receive a wrong command");
                        }
                    }
                }
            }
        });
    }
}

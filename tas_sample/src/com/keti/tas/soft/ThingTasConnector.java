package com.keti.tas.soft;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import jssc.SerialPort;
import jssc.SerialPortException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



/**
 *
 * @author ChenNan
 */
public class ThingTasConnector extends TasAbstractServer{
    
    private final String seriaPortName = "/dev/ttySAC3";
    private final SerialPort serialPort;
    private boolean isActive = false;
    private boolean isSplitFrame = false;
    private boolean isEndFrame = true;
//    private boolean endFrame = false;
    private ArrayList<Byte> tempData = new ArrayList<Byte>();
    
    public ThingTasConnector(){
    	serialPort = new SerialPort(seriaPortName);
    }
    
    @Override
    public void start(){
        isActive = true;
        
        System.out.println(KoreaTimeZone.getDisplayTimeNow() + " => Info: A TAS server for Thing is running now!");
        
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                	serialPort.openPort();
                	serialPort.setParams(SerialPort.BAUDRATE_115200,
                            SerialPort.DATABITS_8, 
                            SerialPort.STOPBITS_1, 
                            SerialPort.PARITY_NONE);

                    while(isActive){
                        byte[] buffer = serialPort.readBytes(18); //receive data length for one time
                        
                        for(int i = 0; i < buffer.length; i++){
                            if(buffer[i] == 0x7e){
                                isSplitFrame = true;
                                
                                if(!isEndFrame){
                                    isEndFrame = true;
                                    
                                    String strData = byteArrayToHex(makeBufferChangeFlagFrame(tempData));
                                    
                                    activeReceiveEvent(strData);
                                }
                                continue;
                            }
                            
                            if(isSplitFrame && buffer[i] == 0x42){
                                isSplitFrame = false;
                                isEndFrame = false;
                                tempData.clear();
                                continue;
                            }
                            
                            tempData.add(new Byte(buffer[i]));
                            isSplitFrame = false;
                        }

                    }
                } catch (SerialPortException ex) {
                    Logger.getLogger(ThingTasConnector.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }).start();    
    }
    
    @Override
    public void stop(){
        isActive = false;
    }
    
    public void sendToThing(byte[] comm){
        try {
        	serialPort.writeBytes(comm);
        } catch (SerialPortException ex) {
            Logger.getLogger(ThingTasConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private byte[] makeBufferChangeFlagFrame(ArrayList<Byte> buffer){
        ArrayList<Byte> data = new ArrayList<Byte>();
        
        for(int i = 0; i < buffer.size(); i++){
            if(i < buffer.size() - 1){
                if(buffer.get(i).byteValue() == 0x7d
                        && buffer.get(i + 1).byteValue() == 0x5e)
                {
                    byte value = 0x7e;
                    data.add(new Byte(value));
                    System.out.println("Replace a 0x7d 0x5e");
                    i++;
                    continue;
                }
                
                if(buffer.get(i).byteValue() == 0x7d
                        && buffer.get(i + 1).byteValue() == 0x5d)
                {
                    byte value = 0x7d;
                    data.add(new Byte(value));
                    System.out.println("Replace a 0x7d 0x5e");
                    i++;
                    continue;
                }

            }
            data.add(buffer.get(i).byteValue());
        }
        
        byte[] newBuff = new byte[data.size()];
        
        for(int i = 0; i < data.size(); i++){
            newBuff[i] = data.get(i).byteValue();
        }
        
        return newBuff;
    }
    
    public static String byteArrayToHex(byte[] ba) {
        if (ba == null || ba.length == 0) {
                return null;
        }

        StringBuffer sb = new StringBuffer(ba.length * 2);
        String hexNumber;
        for (int x = 0; x < ba.length; x++) {
            hexNumber = "0" + Integer.toHexString(0xff & ba[x]);
            sb.append(hexNumber.substring(hexNumber.length() - 2));
        }
        return sb.toString();
    }
    
    public static byte[] hexToByteArray(String hex) { 
        if (hex == null || hex.length() == 0) { 
                return null; 
        } 

        byte[] ba = new byte[hex.length() / 2]; 
        for (int i = 0; i < ba.length; i++) { 
                ba[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16); 
        } 
        return ba; 
    }
}

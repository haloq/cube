package kr.re.keti.ae;

import java.net.URI;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class InteractionRequest {
	public static final String aeInternalIpAddress = "203.254.173.133";
	
	public static String aeRegistrationMessage(String aeName, String appId) throws Exception {
		String requestBody = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
							"<m2m:AE xmlns:m2m=\"http://www.onem2m.org/xml/protocols\">\n" + 
							"<App-ID>" + appId + "</App-ID>\n" +
							"</m2m:AE>";

		StringEntity entity = new StringEntity(
				new String(requestBody.getBytes()));

		URI uri = new URIBuilder()
				.setScheme("http")
				.setHost(aeInternalIpAddress)
				.setPath("/nCube")
				.setParameter("ty", "AE")
				.setParameter("nm", aeName)
				.build();
		
		HttpPost post = new HttpPost(uri);
				post.setHeader("From", "localhost");
				post.setHeader("X-M2M-RI", "0001");
				post.setHeader("Accept", "application/onem2m-resource+xml");
				post.setHeader("Content-Type", "application/onem2m-resource+xml");
				post.setEntity(entity);
		
		CloseableHttpClient httpClient = HttpClients.createDefault();
		
		HttpResponse response = httpClient.execute(post);
		
		int responseCode = response.getStatusLine().getStatusCode();
		
		System.out.println("Return Http response code : " + responseCode);
		
		HttpEntity responseEntity = response.getEntity();
		
		String responseString = EntityUtils.toString(responseEntity);
		
		System.out.println("Return Http response body : " + responseString);
		
		httpClient.close();
		
		return responseString;
	}
	
	public static String containerCreateMessage(String appId, String containerName) throws Exception {
		String requestBody = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
							"<m2m:container xmlns:m2m=\"http://www.onem2m.org/xml/protocols\">\n" + 
							"<containerType>testType</containerType>\n" +
							"</m2m:container>";

		StringEntity entity = new StringEntity(
				new String(requestBody.getBytes()));

		URI uri = new URIBuilder()
				.setScheme("http")
				.setHost(aeInternalIpAddress)
				.setPath("/nCube/AE-" + appId)
				.setParameter("ty", "container")
				.setParameter("nm", containerName)
				.build();
		
		HttpPost post = new HttpPost(uri);
				post.setHeader("From", "localhost");
				post.setHeader("X-M2M-RI", "0001");
				post.setHeader("Accept", "application/onem2m-resource+xml");
				post.setHeader("Content-Type", "application/onem2m-resource+xml");
				post.setEntity(entity);
		
		CloseableHttpClient httpClient = HttpClients.createDefault();
		
		HttpResponse response = httpClient.execute(post);
		
		int responseCode = response.getStatusLine().getStatusCode();
		
		System.out.println("Return Http response code : " + responseCode);
		
		HttpEntity responseEntity = response.getEntity();
		
		String responseString = EntityUtils.toString(responseEntity);
		
		System.out.println("Return Http response body : " + responseString);
		
		httpClient.close();
		
		return responseString;
	}
	
	public static String contentInstanceCreateMessage(String appId, String containerName, String content) throws Exception {
		String requestBody = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
							"<m2m:contentInstance xmlns:m2m=\"http://www.onem2m.org/xml/protocols\">\n" + 
							"<typeOfContent>String</typeOfContent>\n" + 
							"<content>" + content + "</content>\n" +
							"<linkType>no</linkType>\n" +
							"</m2m:contentInstance>";

		StringEntity entity = new StringEntity(
				new String(requestBody.getBytes()));

		URI uri = new URIBuilder()
				.setScheme("http")
				.setHost(aeInternalIpAddress)
				.setPath("/nCube/AE-" + appId + "/container-" + containerName)
				.setParameter("ty", "contentInstance")
				.build();
		
		HttpPost post = new HttpPost(uri);
				post.setHeader("From", "localhost");
				post.setHeader("X-M2M-RI", "0001");
				post.setHeader("Accept", "application/onem2m-resource+xml");
				post.setHeader("Content-Type", "application/onem2m-resource+xml");
				post.setEntity(entity);
		
		CloseableHttpClient httpClient = HttpClients.createDefault();
		
		HttpResponse response = httpClient.execute(post);
		
		int responseCode = response.getStatusLine().getStatusCode();
		
		System.out.println("Return Http response code : " + responseCode);
		
		HttpEntity responseEntity = response.getEntity();
		
		String responseString = EntityUtils.toString(responseEntity);
		
		System.out.println("Return Http response body : " + responseString);
		
		httpClient.close();
		
		return responseString;
	}
}
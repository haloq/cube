package kr.re.keti.ae;

public class AeTest {
	
	static final String aeName = "oneM2MAE";
	static final String appId = "0.2.481.2.0001.001.369";
	static final String containerName = "test";
	static final String content = "101";
	
	public static void main(String args[]) throws Exception {
		System.out.println("[ID-AE] AE start");
		
		System.out.println("[ID-AE] AE Create request : " + aeName);
		InteractionRequest.aeRegistrationMessage(aeName, appId);
		
		System.out.println("[ID-AE] AE container Create request" + containerName);
		InteractionRequest.containerCreateMessage(appId, containerName);
		
		System.out.println("[ID-AE] AE contentInstance Create request");
		InteractionRequest.contentInstanceCreateMessage(appId, containerName, content);
		
		while(true) {
			
		}
	}
}
package com.einsware.simple.plenty;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.app.AlertDialog;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

public class ConnectionManager {

	public static final String TAG = "ConnectionManager" ;
	
	private static final String ENC_TYPE = "euc-kr" ;
	
	public static final String SERVER_URL = "http://open.iotmobius.com" ;
	
	public static final String USER_LOGIN          = SERVER_URL + "/Mobius?division=user&function=login" ;
	public static final String USER_LOGOUT         = SERVER_URL + "/Mobius?division=user&function=logout" ;
	
	public static final int CONN_TIMEOUT = 15*1000 ;
	
	public  static final String DEVICE_ID			= "0.2.481.1.0001.001.975" ;
	public static final String DEVICE_GET_API		= "ledstate" ;
	public static final String DEVICE_CONTROL_API	= "ledcontrol" ;
	
	public static final String LED_ON				= "led,on" ;
	public static final String LED_OFF				= "led,off" ;
	
	public static String putServerLogInOut (String strURL, int option, 
			String strUser, String strPassword, String strServerUKey)
	{
		String strRet = "" ;
		
		try
		{
			String urlString = strURL ;
			
			Log.d(TAG, "put url : " + urlString) ;
			
			HttpClient httpClient = new DefaultHttpClient () ;
						
			HttpParams httpParams = httpClient.getParams() ;
			httpParams.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1) ;
			HttpConnectionParams.setConnectionTimeout(httpParams, CONN_TIMEOUT) ;
			HttpConnectionParams.setSoTimeout(httpParams, CONN_TIMEOUT) ;
			
			HttpResponse response = null ;
			
			HttpPut putData = new HttpPut (urlString) ;
			putData.setHeader("Content-Type", "application/xml; charset=UTF-8");
			
			if (option == 0)
			{
				putData.addHeader("user_id", strUser) ;
				putData.addHeader("password", strPassword) ;
				
				Log.d(TAG, "user_id : " + strUser) ;// + ", password : " + strCurrPassword) ;
			}
			else if (option == 1)
			{
				if (strServerUKey == null)
					strServerUKey = "" ;
				
				putData.addHeader("u_key", strServerUKey) ;
			}
			
			putData.addHeader("locale", "ko") ;
			
			response = httpClient.execute(putData) ;
			
			if (response != null)
			{
				HttpEntity resEntity = response.getEntity() ;
				
				if (resEntity != null)
				{
					strRet = EntityUtils.toString(resEntity) ;
					
					Log.d(TAG, strRet) ;
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace() ;
		}
		
		return strRet ;
	}
	
	public static String getServerData (String strURL, String strServerUKey)
	{
		String strRet = "" ;
		
		try
		{
			String urlString = strURL ;

			Log.d("Connect GET", urlString) ;
			
			HttpClient httpClient = new DefaultHttpClient () ;
			
			HttpParams httpParams = httpClient.getParams() ;
			httpParams.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1) ;
			HttpConnectionParams.setConnectionTimeout(httpParams, CONN_TIMEOUT) ;
			HttpConnectionParams.setSoTimeout(httpParams, CONN_TIMEOUT) ;
			
			HttpResponse response = null ;
			
			HttpGet getData = new HttpGet (urlString) ;
			getData.setHeader("Accept", "application/onem2m-resource+xml") ;//; charset=UTF-8");
			getData.setHeader("Content-Type", "application/onem2m-resource+xml") ;//; charset=UTF-8");
			
			if (strServerUKey == null)
				strServerUKey = "" ;
			
			getData.addHeader("uKey", strServerUKey) ;
			
			Log.d("GET Param", "uKey : " + strServerUKey) ;
			
			getData.addHeader("locale", "ko") ;
			getData.addHeader("From", "http://www.einsware.com/") ;
			getData.addHeader("X-M2M-RI", "12345") ;
			
			response = httpClient.execute(getData) ;
			
			if (response != null)
			{
				HttpEntity resEntity = response.getEntity() ;
				
				if (resEntity != null)
				{
					strRet = EntityUtils.toString(resEntity) ;
					Log.d("Connect GET", strRet) ;
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace() ;
		}
		
		return strRet ;
	}
	
	public static String postServerData (String strURL, String strServerUKey, String strSendData)
	{
		String strRet = "" ;
		
		try
		{
			String urlString = strURL ;

			Log.d("Connect POST", urlString) ;
			
			HttpClient httpClient = new DefaultHttpClient () ;
			
			HttpParams httpParams = httpClient.getParams() ;
			httpParams.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1) ;
			HttpConnectionParams.setConnectionTimeout(httpParams, CONN_TIMEOUT) ;
			HttpConnectionParams.setSoTimeout(httpParams, CONN_TIMEOUT) ;
			
			HttpResponse response = null ;
			
			HttpPost postData = new HttpPost (urlString) ;
			postData.setHeader("Accept", "application/onem2m-resource+xml") ;//; charset=UTF-8");
			postData.setHeader("Content-Type", "application/onem2m-resource+xml") ;//; charset=UTF-8");
			
			if (strServerUKey == null)
				strServerUKey = "" ;
			
			postData.addHeader("uKey", strServerUKey) ;
			
			Log.d("POST Param", "uKey : " + strServerUKey) ;
			
			postData.addHeader("locale", "ko") ;
			postData.addHeader("From", "http://www.einsware.com/") ;
			postData.addHeader("X-M2M-RI", "12345") ;
			
			if (strSendData != null && strSendData.length() > 0)
			{
				postData.setEntity(new StringEntity (strSendData, "UTF-8"));
			}
			
			response = httpClient.execute(postData) ;
			
			if (response != null)
			{
				HttpEntity resEntity = response.getEntity() ;
				
				if (resEntity != null)
				{
					strRet = EntityUtils.toString(resEntity) ;
					Log.d("Connect POST", strRet) ;
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace() ;
		}
		
		return strRet ;
	}

	public static String getDeviceDataURL()
	{
		String strRet = "" ;
		
		strRet = SERVER_URL + "/Mobius/remoteCSE-" + DEVICE_ID + "/container-" + DEVICE_GET_API + "/latest" ;
		strRet += "?countPerPage=10&startIndex=1" ;
		
		return strRet ;
	}
	
	public static String getDeviceControlURL ()
	{
		String strRet = "" ;
		
		strRet = SERVER_URL + "/Mobius/remoteCSE-" + DEVICE_ID + "/mgmtCmd-" + DEVICE_CONTROL_API ;
		strRet += "?ty=execInstance" ;
		
		return strRet ;
	}
	
	public static String getControlXML (String controlData)
	{
		String strRet = "" ;
		
		strRet = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
				+"<m2m:mgmtCmd xmlns:m2m=\"http://www.onem2m.org/xml/protocols\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
				+ "<execReqArgs>" + controlData + "</execReqArgs>"
				+ "</m2m:mgmtCmd>" ;
		
		return strRet ;
	}
	
	public static void onLogout (final String strServerUKey)
	{
    	Constant.mHandler.postDelayed(new Runnable ()
    	{
			@Override
			public void run() {
				// TODO Auto-generated method stub

				try
				{
					String strRecv = ConnectionManager.putServerLogInOut(ConnectionManager.USER_LOGOUT, 1, "", "", strServerUKey) ;
				}
				catch (Exception e)
				{
					e.printStackTrace() ;
				}
			}
    	}, 500) ;
	}
}

package com.einsware.simple.plenty;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnCancelListener;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends Activity {
	
	private String serverUKey = "" ;
	
	private EditText editID ;
	private EditText editPassword ;
	private TextView textViewData ;
	
	private onGetDeviceDataTask getDeviceDataTask ;
	private onPutDeviceControlTask putDeviceControlTask ;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		editID = (EditText) findViewById (R.id.editID) ;
		editPassword = (EditText) findViewById (R.id.editPassword) ;
		textViewData = (TextView) findViewById (R.id.textViewData) ;
		
		textViewData.setText("");
		
		final Handler mHandler = new Handler () ;
		Timer timer = new Timer () ;
		TimerTask getDeviceDataTimerTask = new TimerTask ()
		{
			@Override
			public void run() {
				// TODO Auto-generated method stub
				mHandler.post(new Runnable ()
				{
					@Override
					public void run()
					{
						// TODO Auto-generated method stub
						try
						{
							//Log.d("Main", "server u key : " + serverUKey) ;
							
							if (serverUKey != null && serverUKey.length() > 0)
							{
								if (getDeviceDataTask != null)
								{
									try
									{
										getDeviceDataTask.cancel(true) ;
									}
									catch (Exception e)
									{
										e.printStackTrace();
									}
								}
								
								getDeviceDataTask = new onGetDeviceDataTask () ;
								getDeviceDataTask.setTextView(textViewData);
								getDeviceDataTask.execute(serverUKey) ;
							}
						}
						catch (Exception e)
						{
							e.printStackTrace();
						}
					}
				}) ;
			}
		} ;
		timer.schedule(getDeviceDataTimerTask, 0, 5*1000);
	}
	
	public void onLogin (View view)
	{
		new onLoginTask ().execute(editID.getText().toString(), editPassword.getText().toString()) ;
	}
	
	public void onControlBtn (View view)
	{
		String strSendData = "" ;
		
		if (((ToggleButton) view).isChecked())
		{
			strSendData = ConnectionManager.LED_ON ;
		}
		else
		{
			strSendData = ConnectionManager.LED_OFF ;
		}
		
		textViewData.append("\n... Send data : " + strSendData);
		
		if (putDeviceControlTask != null)
		{
			try
			{
				putDeviceControlTask.cancel(true) ;
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		
		putDeviceControlTask = new onPutDeviceControlTask () ;
		putDeviceControlTask.setTextView(textViewData);
		putDeviceControlTask.execute(serverUKey, strSendData) ;
	}
	
	private class onLoginTask extends AsyncTask <String, Void, String>
	{
    	private ProgressDialog progressDlg ;

		@Override
		protected void onPreExecute ()
		{
			super.onPreExecute() ;
			
			progressDlg = new ProgressDialog (MainActivity.this) ;
			progressDlg.setMessage("Wait for a second.") ;
			progressDlg.setIndeterminate(true) ;
			progressDlg.setProgressStyle(ProgressDialog.STYLE_SPINNER) ;
			progressDlg.setCancelable(false) ;
			progressDlg.show() ;
			
			try
			{
				ConnectionManager.onLogout(serverUKey);
			}
			catch (Exception e)
			{}
		}

		@Override
		protected String doInBackground(String... arg0) {
			// TODO Auto-generated method stub
			
			String strUserID = arg0[0] ;
			String strPassword = arg0[1] ;
			String strUKey = "" ;

			
			if (strUserID == null || strUserID.trim().length() < 1)
				return "" ;

			try
			{
				String strRecv = ConnectionManager.putServerLogInOut(ConnectionManager.USER_LOGIN, 0, strUserID, strPassword, "") ;

				try
				{
					DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance() ;
					DocumentBuilder docBuilder = factory.newDocumentBuilder() ;
					
					InputStream is = new ByteArrayInputStream (strRecv.getBytes()) ;
					
					Document doc = docBuilder.parse(is) ;
					NodeList resultNodes = doc.getElementsByTagName("mobius") ;
					
					for (int i=0; i<resultNodes.getLength(); i++)
					{
						Element subElement = (Element) resultNodes.item(i) ;
						
						try
						{
							strUKey = DataManager.getElementXMLData (subElement, "u_key") ;
					        
					        System.gc() ;
						}
						catch (Exception e)
						{
							e.printStackTrace() ;
						}
					}
					
					is.close() ;
				}
				catch (Exception e)
				{
					e.printStackTrace() ;
				}
			}
			catch (Exception e)
			{
				e.printStackTrace() ;
			}

			return strUKey ;
		}
		
		@Override
		protected void onPostExecute (String strRecv)
		{
			super.onPostExecute(strRecv);
			
			progressDlg.dismiss();
			
			if (strRecv.trim().length() > 0)
			{
				serverUKey = strRecv ;
			}
			else
			{
				Constant.ALERT_ONE(MainActivity.this, "로그인 실패 !!!", "다시 로그인해 주세요 !!!");
			}
		}
	}

	private class onGetDeviceDataTask extends AsyncTask <String, Void, String>
	{
    	private ProgressDialog progressDlg ;
    	private TextView textView ;
    	
    	public void setTextView (TextView mainTextView)
    	{
    		textView = mainTextView ;
    	}

		@Override
		protected void onPreExecute ()
		{
			super.onPreExecute() ;
			
			progressDlg = new ProgressDialog (MainActivity.this) ;
			progressDlg.setMessage("Wait for a second.") ;
			progressDlg.setIndeterminate(true) ;
			progressDlg.setProgressStyle(ProgressDialog.STYLE_SPINNER) ;
			progressDlg.setCancelable(false) ;
			progressDlg.show() ;
		}

		@Override
		protected String doInBackground(String... arg0) {
			// TODO Auto-generated method stub
			
			String strUKey = arg0[0] ;
			String strRecvData = "" ;
			
			if (strUKey == null || strUKey.trim().length() < 1)
				return "" ;
			
			try
			{
				String strDataURL = ConnectionManager.getDeviceDataURL() ;
				String strRecv = ConnectionManager.getServerData(strDataURL, strUKey) ;
				Log.d("Recv Data", strRecv) ;

				try
				{
					DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance() ;
					DocumentBuilder docBuilder = factory.newDocumentBuilder() ;
					
					InputStream is = new ByteArrayInputStream (strRecv.getBytes()) ;
					
					Document doc = docBuilder.parse(is) ;
					NodeList resultNodes = doc.getElementsByTagName("contentInstance") ;
					
					for (int i=0; i<resultNodes.getLength(); i++)
					{
						Element subElement = (Element) resultNodes.item(i) ;
						
						try
						{
							strRecvData = DataManager.getElementXMLData (subElement, "content") ;
							
					        System.gc() ;
						}
						catch (Exception e)
						{
							e.printStackTrace() ;
						}
					}
					
					is.close() ;
				}
				catch (Exception e)
				{
					e.printStackTrace() ;
				}
			}
			catch (Exception e)
			{
				e.printStackTrace() ;
			}

			return strRecvData ;
		}
		
		@Override
		protected void onPostExecute (String strRecv)
		{
			super.onPostExecute(strRecv);
			
			progressDlg.dismiss();
			
			if (strRecv.trim().length() > 0)
			{
				textView.append("\n... Get Data [" + strRecv + "]");
			}
		}
	}

	private class onPutDeviceControlTask extends AsyncTask <String, Void, String>
	{
    	private ProgressDialog progressDlg ;
    	private TextView textView ;
    	
    	public void setTextView (TextView mainTextView)
    	{
    		textView = mainTextView ;
    	}

		@Override
		protected void onPreExecute ()
		{
			super.onPreExecute() ;
			
			progressDlg = new ProgressDialog (MainActivity.this) ;
			progressDlg.setMessage("Wait for a second.") ;
			progressDlg.setIndeterminate(true) ;
			progressDlg.setProgressStyle(ProgressDialog.STYLE_SPINNER) ;
			progressDlg.setCancelable(false) ;
			progressDlg.show() ;
		}

		@Override
		protected String doInBackground(String... arg0) {
			// TODO Auto-generated method stub
			
			String strUKey = arg0[0] ;
			String strSendData = arg0[1] ;
			String strRecvCode = "" ;
			
			String strControlXML = ConnectionManager.getControlXML(strSendData) ;
			
			if (strUKey == null || strUKey.trim().length() < 1)
				return "" ;
			
			try
			{
				String strDataURL = ConnectionManager.getDeviceControlURL() ;
				String strRecv = ConnectionManager.postServerData(strDataURL, strUKey, strControlXML) ;
				Log.d("Recv Data", strRecv) ;

				try
				{
					DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance() ;
					DocumentBuilder docBuilder = factory.newDocumentBuilder() ;
					
					InputStream is = new ByteArrayInputStream (strRecv.getBytes()) ;
					
					Document doc = docBuilder.parse(is) ;
					NodeList resultNodes = doc.getElementsByTagName("m2m:execInstance") ;
					
					for (int i=0; i<resultNodes.getLength(); i++)
					{
						Element subElement = (Element) resultNodes.item(i) ;
						
						try
						{
							strRecvCode = DataManager.getElementXMLData (subElement, "resourceID") ;
							
					        System.gc() ;
						}
						catch (Exception e)
						{
							e.printStackTrace() ;
						}
					}
					
					is.close() ;
				}
				catch (Exception e)
				{
					e.printStackTrace() ;
				}
			}
			catch (Exception e)
			{
				e.printStackTrace() ;
			}

			return strRecvCode ;
		}
		
		@Override
		protected void onPostExecute (String strRecv)
		{
			super.onPostExecute(strRecv);
			
			progressDlg.dismiss();
			
			if (strRecv.trim().length() > 0)
			{
				textView.append("\n--------- Recv Device Control Result Code ---------");
				textView.append("\n" + strRecv);
				textView.append("\n-----------------------------------------");
			}
		}
	}
}

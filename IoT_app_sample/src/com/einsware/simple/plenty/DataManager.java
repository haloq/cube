package com.einsware.simple.plenty;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.util.Log;

public class DataManager {

	public static final String TAG = "DataManager" ;
	
	public static String getElementXMLData (Element elem, String strTag)
	{
		String strRet = "" ;
		
		NodeList nodeData = null ;
		
		if (elem != null)
		{
			try
			{
				nodeData = elem.getElementsByTagName(strTag) ;
			}
			catch (Exception e)
			{
				e.printStackTrace() ;
			}
		}
		
		if (nodeData != null)
		{
			try
			{
				Node rootNode = nodeData.item(0) ;
				Node childNode = null ;
				
				if (rootNode != null)
				{
					childNode = rootNode.getFirstChild() ;
				}
				
				if (childNode != null)
				{
					strRet = childNode.getNodeValue() ;
				}
			}
			catch (Exception e)
			{
				e.printStackTrace() ;
			}
		}
		
		return strRet ;
	}
	
	public static String getStrDataFromNode (NodeList nodeData)
	{
		String strRet = "" ;
		
		try
		{
			strRet = nodeData.item(0).getFirstChild().getNodeValue() ;
		}
		catch (Exception e)
		{
			e.printStackTrace() ;
		}
		
		return strRet ;
	}
}
